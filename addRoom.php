<?php
    session_start();
    var_dump($_POST);
    var_dump($_SESSION);
    if(isset($_POST["name"]) and isset($_SESSION["username"]))
    {
        $dbHost = '127.0.0.1';
        $dbName = 'Baint';
        $dbUser = 'root';
        $dbPass = '';
        $dbCharset = 'utf8mb4';
        $dbOptions = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];
        $dsn = "mysql:host=$dbHost;dbname=$dbName;charset=$dbCharset";
        try
        {
            $pdo = new PDO($dsn, $dbUser, $dbPass, $dbOptions);
        }
        catch (\PDOException $e)
        {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }

        $stmt = $pdo->prepare("SELECT name FROM rooms WHERE name=:name");
        $stmt->bindParam("name",$_POST["name"]);
        $stmt->execute();
        if($stmt->fetch())
        {
            header("location: /progetto_bello/paint.php?idRoom=$name&message=roomExists");
        }
        $stmt = $pdo->prepare("INSERT INTO rooms(name,owner) VALUES (:name,:owner)");
        $stmt->bindParam("name",$_POST["name"]);
        $stmt->bindParam("owner",$_SESSION["username"]);
        $stmt->execute();
        header("location: /progetto_bello/paint.php?roomId=".$_POST['name']."&message=roomCreated");

    }
    else
    {
        header("location: /progetto_bello/start.php");
    }


?>
