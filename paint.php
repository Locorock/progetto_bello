<!doctype="html">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="style.css" rel="stylesheet">

<script src="/progetto_bello/libs/jscolor.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<body class="locked-y">
<?php
    $pdo = null;
    $dbHost = '127.0.0.1';
    $dbName = 'Baint';
    $dbUser = 'root';
    $dbPass = '';
    $dbCharset = 'utf8mb4';
    $dbOptions = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];
    $dsn = "mysql:host=$dbHost;dbname=$dbName;charset=$dbCharset";
    try
    {
        $pdo = new PDO($dsn, $dbUser, $dbPass, $dbOptions);
    }
    catch (\PDOException $e)
    {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }

    session_start();
    //Controlla se è impostato un messaggio di sistema e stampa un allerta
    if(isset($_GET["message"]))
    {
        if($_GET["message"]=="logout")
        {
            unset($_SESSION["authToken"]);
            unset($_SESSION["username"]);
            $auth = false;
            echo "<script>
                    Swal.fire(
                      'Success',
                      'Logout eseguito con successo',
                      'success'
                    )
                  </script>";
        }
        if($_GET["message"]=="roomExists")
        {
            echo "<script>
                    Swal.fire(
                      'Errore',
                      'Stanza già esistente',
                      'error'
                    )
                  </script>";
        }
        if($_GET["message"]=="roomCreated")
        {
            echo "<script>
                    Swal.fire(
                      'Successo',
                      'Stanza creata',
                      'success'
                    )
                  </script>";
        }
    }
    if(!isset($_SESSION["authToken"]))
    {
        $auth=false;
    }
    else
    {
        $auth=true;
    }
?>


<p hidden id="roomId" value="<?php if(isset($_GET["roomId"])){echo $_GET['roomId'];} ?>"></p>
<p hidden id="authToken" value="<?php if(isset($_SESSION["authToken"])){echo $_SESSION['authToken'];} ?>"></p>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="start.php"><img src="logo.png" class="icon" width="30" height="30"></img> Baint</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/progetto_bello/info.php">Info</a>
            </li>
            <?php //Dropdown menu con lista delle ultime 10 stanze visitate
                if($auth)
                {
                    echo '<li class="nav-item dropdown">
                             <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 Recent Rooms
                             </a>
                             <div class="dropdown-menu" aria-labelledby="navbarDropdown">';

                    $stmt = $pdo->prepare("SELECT DISTINCT room FROM partecipations WHERE user= :username ORDER BY timestamp LIMIT 10");
                    $stmt->bindParam("username",$_SESSION["username"]);
                    $stmt->execute();
                    while($row = $stmt->fetch())
                    {
                        $room = $row["room"];
                        echo '<a class="dropdown-item" href="paint.php?roomId='.$room.'">'.$room.'</a>';
                    }
                }
            ?>


        </li></ul>
        <ul class="navbar-nav navbar-right">
            <?php //Stampa i campi login e sign up se non loggato oppure il campi inserisci stanza, cancella stanza e logout se loggato
                if(!$auth)
                {
                    echo
                    '
                    <li class="nav-item"> <a href="register.php" class="btn btn-info my-2 my-sm-0 buttonspaced" role="button">Sign Up</a> </li>
                    <div class="nav-item btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle buttonspaced pull-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</button>
                        <div class="dropdown-menu dropdown-login dropdown-menu-right">
                            <form class="px-2 py-0" method="POST" action="login.php">
                                <div class="form-group">
                                    <label for="formUser">Username</label>
                                    <input type="username" class="form-control" id="dropdownFormUsername" placeholder="Username" name="username">
                                </div>
                                <div class="form-group">
                                    <label for="formPass">Password</label>
                                    <input type="password" class="form-control" id="dropdownFormPassword" placeholder="Password" name="password">
                                </div>
                                <button type="submit" class="btn btn-primary">Sign in</button>
                            </form>
                        </div>
                    </div>';
                }
                else
                {
                    echo
                    '<div class="nav-item btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle buttonspaced pull-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-plus">
                        </i> Nuova stanza</button>
                        <div class="dropdown-menu dropdown-login dropdown-menu-right">
                            <form class="px-2 py-0" method="POST" action="addRoom.php">
                                <div class="form-group">
                                    <label for="formName">Nome</label>
                                    <input type="name" class="form-control" id="dropdownFormName" placeholder="Nome" name="name">
                                </div>
                                <button type="submit" class="btn btn-primary">Crea stanza</button>
                            </form>
                        </div>
                    </div>';

                    if(isset($_GET["roomId"]))
                    {
                        $stmt = $pdo->prepare("SELECT * FROM rooms WHERE owner=:owner AND name=:room");
                        $stmt->bindParam("owner",$_SESSION["username"]);
                        $stmt->bindParam("room",$_GET["roomId"]);
                        $stmt->execute();
                        if($stmt->fetch())
                        {
                            echo
                            '<button class="btn btn-danger buttonspaced" id="deleteButton">Cancella Stanza</button>';
                        }
                    }

                    echo
                    '<li class="nav-item"> <form class="my-2 my-sm-0" method="GET"> <button type="submit" name="message" value="logout" class="btn btn-danger buttonspaced">Sign Out</button></form> </li>';
                }
            ?>
        </ul>
    </div>
</nav>




<!-- content -->
<?php //Stampa la canvas e i vari controlli se loggato e in una stanza esistente, oppure stampa un messaggio di errore
    if($auth)
    {
        if(isset($_GET["roomId"]))
        {
            $stmt = $pdo->prepare('SELECT * FROM rooms WHERE name=:id');
            $stmt->bindParam("id",$_GET["roomId"]);
            $stmt->execute();
            if($row = $stmt->fetch())
            {
                echo '<div class="container content">
                          <div class="row justify-content-between">
                              <div class="col-2">
                                  Colore: <input class="jscolor smooth" id="color" value="EB0000">
                              </div>
                              <div class="col-2">
                                  <span id="widthLabel">Spessore: 3</span><input type="range" min="1" max="50" value="3" class="slider" id="width">
                              </div>
                              <div class="col-4 text-center">
                                  <label class="h3" style="margin-top: 2vh">'.$_GET["roomId"].'</label>
                              </div>
                              <div class="col-4 text-right" style="margin-top: 2vh; padding-right:5vh;">
                                   <span class="dropdown" style="margin-top: 2.5vh">
                                      <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Utenti
                                      </button>
                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="usersList"></div>
                                  </span>
                                  <button class="btn btn-info" type="button" id="downloadButton"> Download </button>
                                  <button class="btn btn-danger" type="button" id="clearButton"> Pulisci </button>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col">
                                  <canvas id="myCanvas" class="img" width="1100px" height="650px" style="border:2px solid #000000;""></canvas>
                              </div>
                          </div>
                          <div id="lastMessage" style="margin-left:5px;"></div>
                          <script src="canvas.js"></script>
                      </div>';
            }
            else
            {
                echo '<div class="jumbotron vertical-center">
                 <p class="m-0 text-center text-danger h1">Questa stanza non esiste</p>
                 </div>';
            }
        }
        else
        {
            echo '<div class="jumbotron vertical-center">
             <p class="m-0 text-center text-danger h1">Errore non previsto</p>
             </div>';
        }
    }
    else
    {
        echo '<div class="jumbotron vertical-center">
         <p class="m-0 text-center text-danger h1">Per accedere ad una stanza devi prima essere loggato</p>
         </div>';
    }
?>
<!-- /.content -->

<!-- Footer -->
<footer class="footer py-3 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Baint 2019</p>
    </div>
    <!-- /.container -->
</footer>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
