<?php
    session_start();
    if(isset($_POST["name"]) and isset($_SESSION["username"]))
    {
        $dbHost = '127.0.0.1';
        $dbName = 'Baint';
        $dbUser = 'root';
        $dbPass = '';
        $dbCharset = 'utf8mb4';
        $dbOptions = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];
        $dsn = "mysql:host=$dbHost;dbname=$dbName;charset=$dbCharset";
        try
        {
            $pdo = new PDO($dsn, $dbUser, $dbPass, $dbOptions);
        }
        catch (\PDOException $e)
        {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }

        $stmt = $pdo->prepare("SELECT * FROM rooms WHERE owner=:owner AND name=:name");
        $stmt->bindParam("owner",$_SESSION["username"]);
        $stmt->bindParam("name",$_POST["name"]);
        $stmt->execute();
        if($stmt->fetch())
        {
            $stmt = $pdo->prepare("DELETE FROM rooms WHERE name=:name");
            $stmt->bindParam("name",$_POST["name"]);
            $stmt->execute();
            header("location: /progetto_bello/start.php?message=roomDeleted");
        }
        header("location: /progetto_bello/start.php");
    }
    else
    {
        header("location: /progetto_bello/start.php");
    }


?>
