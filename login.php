<?php
    session_start();
    if(isset($_POST["username"]) and isset($_POST["password"]))
    {
        $dbHost = '127.0.0.1';
        $dbName = 'Baint';
        $dbUser = 'root';
        $dbPass = '';
        $dbCharset = 'utf8mb4';
        $dbOptions = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];
        $dsn = "mysql:host=$dbHost;dbname=$dbName;charset=$dbCharset";
        try
        {
            $pdo = new PDO($dsn, $dbUser, $dbPass, $dbOptions);
        }
        catch (\PDOException $e)
        {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }

        $stmt = $pdo->prepare("SELECT * FROM users WHERE username = :username");
        $stmt->bindParam("username",$_POST["username"]);
        $stmt->execute();
        if($row = $stmt->fetch())
        {
            $hash = $row["pass"];
            $pass = $_POST["password"];
            if(password_verify($pass, $hash))
            {
                $token = bin2hex(random_bytes(64));
                $_SESSION["authToken"] = $token;
                $_SESSION["username"] = $_POST["username"];
                $stmt = $pdo->prepare("UPDATE users SET authToken=:token WHERE username=:username");
                $stmt->bindParam("username",$_POST["username"]);
                $stmt->bindParam("token",$token);
                $stmt->execute();
                header("location: /progetto_bello/start.php?message=login");
            }
        }
        else
        {
            header("location: /progetto_bello/start.php");
        }
    }
    else
    {
        header("location: /progetto_bello/start.php");
    }


?>
