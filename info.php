<!doctype="html">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="style.css" rel="stylesheet">

<body>
<?php
    if(!isset($_SESSION["authToken"]))
    {
        $auth=false;
    }
    else
    {
        $auth=true;
    }
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="start.php"><img src="logo.png" class="icon" width="30" height="30"></img> Baint</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/progetto_bello/info.php">Info</a>
            </li>
        </ul>
        <?php //Stampa i campi login e sign up se non loggato oppure i campi inserisci stanza e logout se loggato
            if(!$auth)
            {
                echo
                '<li class="nav-item"> <a href="register.php" class="btn btn-info my-2 my-sm-0 buttonspaced" role="button">Sign Up</a> </li>
                <div class="nav-item btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle buttonspaced pull-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</button>
                    <div class="dropdown-menu dropdown-login dropdown-menu-right">
                        <form class="px-2 py-0" method="POST" action="login.php">
                            <div class="form-group">
                                <label for="formUser">Username</label>
                                <input type="username" class="form-control" id="dropdownFormUsername" placeholder="Username" name="username">
                            </div>
                            <div class="form-group">
                                <label for="formPass">Password</label>
                                <input type="password" class="form-control" id="dropdownFormPassword" placeholder="Password" name="password">
                            </div>
                        </form>
                    </div>
                </div>';
            }
            else
            {
                echo
                '<div class="nav-item btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle buttonspaced pull-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-plus">
                    </i> Nuova stanza</button>
                    <div class="dropdown-menu dropdown-login dropdown-menu-right">
                        <form class="px-2 py-0" method="POST" action="addRoom.php">
                            <div class="form-group">
                                <label for="formName">Nome</label>
                                <input type="name" class="form-control" id="dropdownFormName" placeholder="Nome" name="name">
                            </div>
                            <button type="submit" class="btn btn-primary">Crea stanza</button>
                        </form>
                    </div>
                </div>';
                echo
                '<li class="nav-item"> <form class="my-2 my-sm-0" method="GET"> <button type="submit" name="message" value="logout" class="btn btn-danger buttonspaced">Sign Out</button></form> </li>';
            }
        ?>
    </div>
</nav>

<div class="jumbotron">
    <div class="container">
        <span class="display-2"><img src="/progetto_bello/logo.png" class="align-right" width="80" height="80"></img>aint</span>
        <div>Un progetto bello che ti permette di disegnare cose con i tuoi amici sull'internet</div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h2>Disegna su un incredible canvas</h2>
            <div>Pittura con spessore variabile e migliaia di colori disponibili</div>
        </div>
        <div class="col-md-4">
            <h2>Completamente sicuro</h2>
            <small>Relativamente parlando</small>
        </div>
        <div class="col-md-4">
            <h2>Registrati ora ed accedi a centinaia* di stanze</h2>
            <small>*una dozzina forse</small>
        </div>
    </div>
</div>
<hr>
<footer class="footer py-3 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Baint 2017</p>
    </div>
</footer>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
