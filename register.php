<!doctype="html">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="style.css" rel="stylesheet">

<body>
<?php
    if(!isset($_SESSION["authToken"]))
    {
        $auth=false;
    }
    else
    {
        $auth=true;
    }
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="start.php"><img src="logo.png" class="icon" width="30" height="30"></img> Baint</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/progetto_bello/info.php">Info</a>
            </li>
        </ul>
        <?php //Stampa i campi login e sign up se non loggato oppure i campi inserisci stanza e logout se loggato
            if(!$auth)
            {
                echo
                '<li class="nav-item"> <a href="register.php" class="btn btn-info my-2 my-sm-0 buttonspaced" role="button">Sign Up</a> </li>
                <div class="nav-item btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle buttonspaced pull-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</button>
                    <div class="dropdown-menu dropdown-login dropdown-menu-right">
                        <form class="px-2 py-0" method="POST" action="login.php">
                            <div class="form-group">
                                <label for="formUser">Username</label>
                                <input type="username" class="form-control" id="dropdownFormUsername" placeholder="Username" name="username">
                            </div>
                            <div class="form-group">
                                <label for="formPass">Password</label>
                                <input type="password" class="form-control" id="dropdownFormPassword" placeholder="Password" name="password">
                            </div>
                            <button type="submit" class="btn btn-primary">Sign in</button>
                        </form>
                    </div>
                </div>';
            }
            else
            {
                echo
                '<div class="nav-item btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle buttonspaced pull-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-plus">
                    </i> Nuova stanza</button>
                    <div class="dropdown-menu dropdown-login dropdown-menu-right">
                        <form class="px-2 py-0" method="POST" action="addRoom.php">
                            <div class="form-group">
                                <label for="formName">Nome</label>
                                <input type="name" class="form-control" id="dropdownFormName" placeholder="Nome" name="name">
                            </div>
                            <button type="submit" class="btn btn-primary">Crea stanza</button>
                        </form>
                    </div>
                </div>';
                echo
                '<li class="nav-item"> <form class="my-2 my-sm-0" method="GET"> <button type="submit" name="message" value="logout" class="btn btn-danger buttonspaced">Sign Out</button></form> </li>';
            }
        ?>
    </div>
</nav>

<?php
    if(isset($_POST["email"]) && isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["passwordRepeat"]))
    {
        $email = $_POST["email"];
        $username = $_POST["username"];
        $password = $_POST["password"];
        $passwordRepeat = $_POST["passwordRepeat"];

        $dbHost = '127.0.0.1';
        $dbName = 'Baint';
        $dbUser = 'root';
        $dbPass = '';
        $dbCharset = 'utf8mb4';
        $dbOptions = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];
        $dsn = "mysql:host=$dbHost;dbname=$dbName;charset=$dbCharset";
        try
        {
            $pdo = new PDO($dsn, $dbUser, $dbPass, $dbOptions);
        }
        catch (\PDOException $e)
        {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }

        //I vari controlli aggiungono stringhe all'array errori
        $errors = array();

        if($email == "")
        {
            array_push($errors, "mailEmpty");
        }

        if($username == "")
        {
            array_push($errors, "usernameEmpty");
        }

        if($password == "")
        {
            array_push($errors, "passEmpty");
        }

        if($passwordRepeat == "")
        {
            array_push($errors, "passRepeatEmpty");
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            array_push($errors, "mailInvalid");
        }

        if (strlen($username)>25)
        {
            array_push($errors, "usernameInvalid");
        }

        if (strlen($password)>25)
        {
            array_push($errors, "passInvalid");
        }

        $stmt = $pdo->prepare('SELECT * FROM users WHERE username = :name');
        $stmt->bindParam("name",$username);
        $stmt->execute();
        if($row = $stmt->fetchColumn())
        {
            array_push($errors, "usernameExists");
        }

        $stmt = $pdo->prepare('SELECT * FROM users WHERE email = :email');
        $stmt->bindParam("email",$email);
        $stmt->execute();
        if($row = $stmt->fetchColumn())
        {
            array_push($errors, "mailExists");
        }

        if($password!=$passwordRepeat)
        {
            array_push($errors, "passDifferent");
        }

        if(empty($errors))
        {
            $stmt = $pdo->prepare("INSERT INTO users (email,username,pass) VALUES (:email,:username,:password)");
            $stmt->bindParam("email",$email);
            $hash = password_hash($password, PASSWORD_DEFAULT);
            $stmt->bindParam("password",$hash);
            $stmt->bindParam("username",$username);
            $stmt->execute();
            header("location: /progetto_bello/start.php");
        }
    }
?>
<!-- container -->
<div class="container content">
    <form method="POST">
        <div class="form-group">
            <label for="emailInput">Email</label>
            <input type="email" class="form-control" name="email" aria-describedby="emailHelp"> </input>
            <?php
                if(isset($errors))
                {
                    if(in_array("mailExists",$errors))
                    {
                        echo '<small class="form-text text-danger">Questa email è già stata utilizzata</small>';
                    }
                    if(in_array("mailEmpty",$errors))
                    {
                        echo '<small class="form-text text-danger">Questo campo è vuoto</small>';
                    }
                    else
                    {
                        if(in_array("mailInvalid",$errors))
                        {
                            echo '<small class="form-text text-danger">Questa email non è valida</small>';
                        }
                    }
                }
            ?>
        </div>
        <div class="form-group">
            <label for="usernameInput">Username</label>
            <input type="username" class="form-control" name="username"> </input>
            <?php
                if(isset($errors))
                {
                    if(in_array("usernameExists",$errors))
                    {
                        echo '<small class="form-text text-danger">Questo username è già stato utilizzato</small>';
                    }
                    if(in_array("usernameInvalid",$errors))
                    {
                        echo '<small class="form-text text-danger">Questo username non è valido</small>';
                    }
                    if(in_array("usernameEmpty",$errors))
                    {
                        echo '<small class="form-text text-danger">Questo campo è vuoto</small>';
                    }
                }
            ?>
        </div>
        <div class="form-group">
            <label for="passwordInput">Password</label>
            <input type="password" class="form-control" name="password"> </input>
            <?php
                if(isset($errors))
                {
                    if(in_array("passDifferent",$errors))
                    {
                        echo '<small class="form-text text-danger">Le due pass inserite non corrispondono</small>';
                    }
                    if(in_array("passInvalid",$errors))
                    {
                        echo '<small class="form-text text-danger">Questa password non è valida</small>';
                    }
                    if(in_array("passEmpty",$errors))
                    {
                        echo '<small class="form-text text-danger">Questo campo è vuoto</small>';
                    }
                }
            ?>
        </div>
        <div class="form-group">
            <label for="passwordRepeatInput">Repeat Password</label>
            <input type="password" class="form-control" name="passwordRepeat"> </input>
            <?php
                if(isset($errors))
                {
                    if(in_array("passEmpty",$errors))
                    {
                        echo '<small class="form-text text-danger">Questo campo è vuoto</small>';
                    }
                }
            ?>
        </div>
        <button type="submit" class="btn btn-primary">Invia</button>
    </form>
</div>
<!-- /.container -->
<!-- Footer -->
<footer class="footer py-3 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Baint 2019</p>
    </div>
    <!-- /.container -->
</footer>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
