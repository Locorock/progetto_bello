var c = document.getElementById("myCanvas");
var lastMessage = document.getElementById("lastMessage");
c.setAttribute('width', 1100);
c.setAttribute('height', 650);
c.setAttribute('id', 'canvas');

var ctx = c.getContext("2d");
var isDown = false;
var connection;
var room = document.getElementById("roomId").attributes.value.value;
var authToken = document.getElementById("authToken").attributes.value.value;

ctx.setTransform(1, 0, 0, 1, 0, 0);
ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

c.onmousedown = function(event){
    draw(event.pageX - $(this).offset().left, event.pageY - $(this).offset().top, false);
    isDown = true;
}

c.onmouseup = function(event){
    isDown = false;
}

c.onmousemove = function(event){
    draw(event.pageX - $(this).offset().left, event.pageY - $(this).offset().top, isDown);
}

runWebSocket();

document.getElementById("clearButton").addEventListener("click", function(){
    //Codice estremamente complicato che pulisce lo schermo e non ricicla assolutamente funzioni già create
    Swal.fire({
        title: 'Sei sicuro?',
        text: "Stai per pulire la stanza",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, voglio pulire questa stanza'
    }).then((result) => {
        if (result.value) {
            drawPath(0,0,1000,800,"FFFFFF", 1500);
            report(0,0,1000,800,"FFFFFF", 1500);
        }
    })

});

document.getElementById("deleteButton").addEventListener("click", function(){
    Swal.fire({
      title: 'Sei sicuro?',
      text: "Stai per cancellare la stanza irreversibilmente",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, voglio cancellare questa stanza'
    }).then((result) => {
      if (result.value) {
        deleteRoom()
      }
    })
});

document.getElementById("downloadButton").addEventListener("click", function(element){
    download()
});

document.getElementById("width").addEventListener("input", function(){
    document.getElementById("widthLabel").innerHTML = "Spessore: "+document.getElementById("width").value;
});

function draw(x, y, isDown) {
    if (isDown) {
        color = document.getElementById("color").value;
        width = document.getElementById("width").value;
        drawPath(x,y,lastX,lastY,color, width)
        report(x,y,lastX,lastY,color, width)
    }
    lastX = x; lastY = y;
}

function drawPath(x,y,lastX,lastY,color, width)
{
    ctx.beginPath();
    ctx.strokeStyle = "#"+color;
    ctx.lineWidth = width;
    ctx.lineJoin = "round";
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(x, y);
    ctx.closePath();
    ctx.stroke();
}

//Aggiorna la canvas in base a un immagine base64
function updateCanvas(base64)
{
    let image = new Image();
    image.onload = function() {
      ctx.drawImage(image, 0, 0);
    };
    image.src = base64;
}

//Codice che spedisce il messaggio di cancellazione stanza al server paint
function report(x,y,lastX,lastY,color, width)
{
    let obj =
    {
        authToken: authToken,
        req: "drawPath",
        curX: x,
        curY: y,
        lastX: lastX,
        lastY: lastY,
        color: color,
        width: width
    };
    json = JSON.stringify(obj);
    connection.send(json);
}

function deleteRoom()
{
    let obj =
    {
        authToken: authToken,
        req: "deleteRoom"
    };
    json = JSON.stringify(obj);
    connection.send(json);
}

function download(canvas, filename) {
    var lnk = document.createElement('a'),e;
    lnk.href = c.toDataURL("image/png");
    lnk.download = "canvas.png";
    if (document.createEvent) {
        e = document.createEvent("MouseEvents");
        e.initMouseEvent("click", true, true, window,
                         0, 0, 0, 0, 0, false, false, false,
                         false, 0, null);
        lnk.dispatchEvent(e);
    } else if (lnk.fireEvent) {
        lnk.fireEvent("onclick");
    }
}

function runWebSocket() {
    connection = new WebSocket("ws://127.0.0.1:1337/");
    messages = [];
    connection.onerror = function (event) {
        console.log("cannot connect to server");
    }
    connection.onopen = function (event) {
        console.log("connected to server");
        let json = '{"authToken":"'+authToken+'","req":"connect", "room":"'+room+'"}';
        connection.send(json);
    }
    connection.onmessage = function (event) {
        content = event.data;
        console.log("received from server: " + event.data);
        json = JSON.parse(content);
        if(json.req == "updateCanvas")
        {
            let base64 = json.base64;
            updateCanvas(base64);
        }
        if(json.req == "drawPath")
        {
            drawPath(json.curX,json.curY,json.lastX,json.lastY,json.color,json.width);
        }
        //Tiene gli ultimi 3 messaggi e li stampa in un elemento sotto la canvas
        if(json.req == "message")
        {
            messages.push(json.message);
            if(messages.length>3)
            {
                messages = messages.slice(1,4);
            }
            for(let i=0;i<messages.length;i++)
            {
                if(i==0)
                {
                    lastMessage.innerHTML = messages[i];
                }
                else
                {
                    lastMessage.innerHTML = messages[i]+"<br>"+lastMessage.innerHTML;
                }
            }
        }
        //Aggiorna la lista utenti della stanza
        if(json.req == "updateUsers")
        {
            let users = json.users;
            let usersList = document.getElementById("usersList");
            let inner = '';
            users.forEach(function(element){
                 inner = inner+'<label class="dropdown-item">'+element+'</label>'
            })
            usersList.innerHTML = inner;
        }
        if(json.req == "disconnect")
        {
            window.location.replace("/progetto_bello/start.php?message=roomDeletedByOwner");
        }

    };
}
