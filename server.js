var WebSocketServer = require('websocket').server;
var http = require('http');
var mysql = require('mysql');
const { createCanvas, loadImage } = require('canvas')

var rooms = [];
var canvases = [];

var myCon = mysql.createConnection(
{
  host: "localhost",
  user: "root",
  password: "",
  database: "Baint"
});

//Si connette al database e imposta l'array associativo delle stanze aperte e la loro immagine in salvata in base64
myCon.connect(function(err) {
  if (err) throw err;
  myCon.query("SELECT name,thumb FROM rooms", function (err, result, fields)
  {
      if (err) throw err;
      result.forEach(function(element)
      {
          let connections = [];
          rooms[element.name] = connections;
          canvas = canvasFromBase(element.thumb);
          canvases[element.name] = canvas;
      })
      console.log("Stanze impostate: ")
      console.log(rooms)
      setTimeout(exportCanvases, 30000);
  });
});





var server = http.createServer(function(request, response)
{
  // process HTTP request. Since we're writing just WebSockets
  // server we don't have to implement anything.
});
server.listen(1337, function() { });

// create the server
wsServer = new WebSocketServer(
{
    httpServer: server
});

// WebSocket server
wsServer.on('request', function(request)
{
    var connection = request.accept(null, request.origin);
    connection.room = null;
    connection.username = null;
    connection.auth;

    console.log((new Date()) + ' Connessione da origine: '+ request.origin);

    connection.on('message', function(message)
    {
        if (message.type === 'utf8') {
            let json = message.utf8Data;
            obj = JSON.parse(json);

            //Se la connessione non è inizializzata è richiesto un messaggio per impostarla
            if(connection.room==null)
            {
                if(obj.req=="connect")
                {
                    //Controlla se il token di autenticazione è valido e ne cerca l'username correlato
                    myCon.query("SELECT * FROM users WHERE authToken="+myCon.escape(obj.authToken), function (err, result, fields)
                    {
                        if (err) throw err;
                        if(!isEmpty(result))
                        {
                            connection.auth = obj.authToken;
                            connection.username = result[0]["username"]
                            //Controlla l'esistenza della stanza richiesta nell'array stanza
                            if(rooms[obj.room]==null)
                            {
                                //Controlla l'esistenza nel database della stanza richiesta
                                myCon.query("SELECT * FROM rooms WHERE name="+myCon.escape(obj.room), function (err, result, fields)
                                {
                                    if (err) throw err;
                                    //Aggiunge all'array delle stanze la stanza richiesta se trovata nel database
                                    if(!isEmpty(result))
                                    {
                                        connection.room = obj.room;
                                        let connections = [];
                                        rooms[connection.room] = connections;  //Assegna l'array vuoto di connessioni per questa stanza
                                        let canvas = createCanvas(1100, 650);
                                        addBackground(canvas);
                                        canvases[obj.room] = canvas;
                                        sendCanvas(connection, canvas);
                                        console.log("Aggiungo "+obj.room+" alle stanze");

                                        //Codice duplicato perchè l'interfaccia con mysql è asincrona rispetto al resto del codice e non si avrebbero i risultati in tempo
                                        rooms[connection.room].push(connection)-1;
                                        addPartToDatabase(connection.username, connection.room);
                                        broadcastMessage(connection.room,"L'utente "+connection.username+" si è connesso alla stanza "+connection.room);
                                        broadcastUsers(connection.room);
                                        console.log("Connessione di "+connection.username+" da nuova stanza"+connection.room+" completata");
                                    }
                                });
                            }
                            else
                            {
                                connection.room = obj.room;
                                rooms[connection.room].push(connection)-1;

                                addPartToDatabase(connection.username, connection.room);
                                broadcastMessage(connection.room,"L'utente "+connection.username+" si è connesso alla stanza "+connection.room);
                                broadcastUsers(connection.room);
                                sendCanvas(connection, canvases[connection.room]);
                                console.log("Connessione di "+connection.username+" alla stanza "+connection.room+" completata")
                            }
                        }
                        else
                        {
                            console.log("Connessione non corretta");
                        }
                    });
                }
            }
            else
            {
                if(obj.authToken==connection.auth)
                {
                    if(obj.req=="drawPath")
                    {
                        console.log(obj)
                        draw(canvases[connection.room],"path", obj.curX, obj.curY, obj.lastX, obj.lastY, obj.color, obj.width);
                        broadcastDrawing(connection.room, connection.username, connection, obj.req, obj.curX, obj.curY, obj.lastX, obj.lastY, obj.color, obj.width)
                    }
                    if(obj.req=="deleteRoom")
                    {
                        console.log("Delete room");
                        myCon.query("SELECT owner FROM rooms WHERE name="+myCon.escape(connection.room), function (err, result, fields)
                        {
                            if(connection.username == result[0].owner)
                            {
                                localRoom = connection.room;
                                disconnectRoom(localRoom);
                                //Cancella la stanza dopo 1 secondo mentre aspetta la risposta dei client
                                setTimeout(deleteRoom, 1000, localRoom);
                            }
                        });
                    }
                }
            }
        }
    });

    connection.on('close', function(reasonCode, description) {
        console.log("Utente "+connection.username+" disconnesso dalla stanza "+connection.room);
        if(connection.room!=null)
        {
            for(let i=0;i<rooms[connection.room].length;i++)
            {
                if(rooms[connection.room][i]===connection)
                {
                    broadcastMessage(connection.room,"L'utente "+connection.username+" si è disconnesso dalla stanza "+connection.room);
                    broadcastUsers(connection.room);
                    rooms[connection.room].splice(i, 1);
                }
            }
        }
    });
});


//Disegna un tratto nella canvas interna
function draw(canvas, mode, x, y, lastX, lastY, color, width)
{
    let ctx = canvas.getContext("2d");
    if(mode == "path")
    {
        ctx.beginPath();
        ctx.strokeStyle = "#"+color;
        ctx.lineWidth = width;
        ctx.lineJoin = "round";
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(x, y);
        ctx.closePath();
        ctx.stroke();
    }
}

//Esporta le canvas nel database in formato base64
function exportCanvases()
{
    for (let key in canvases) {
        let value = canvases[key];
        let dataURL = value.toDataURL();
        myCon.query("UPDATE rooms SET thumb='"+dataURL+"' WHERE name = '"+key+"'", function (err, result, fields)
        {
            if (err) throw err;
        });
    }

    console.log("Esportazione delle canvas interne verso il database completata con successo");
    setTimeout(exportCanvases, 30000);
}

//Invia la canvas a un utente in formato base64
function sendCanvas(connection, canvas)
{
    let dataURL = canvas.toDataURL();
    let obj =
    {
        req: "updateCanvas",
        base64: dataURL
    };
    let json = JSON.stringify(obj);
    connection.sendUTF(json);
    console.log("Inviata canvas interna ad un utente");
}

function addBackground(canvas)
{
    ctx = canvas.getContext("2d");
    ctx.beginPath();
    ctx.rect(0, 0, 1100, 650);
    ctx.fillStyle = "#FFFFFF";
    ctx.fill();
}

//Trasforma un codice base64 in un oggetto canvas
function canvasFromBase(base64)
{
    let localCanvas = createCanvas(1100, 650);
    let localCtx = localCanvas.getContext("2d");
    loadImage(base64).then((image) => {
        localCtx.drawImage(image, 0, 0)
    })
    return localCanvas;
}

function deleteRoom(room)
{
    myCon.query("DELETE FROM rooms WHERE name="+myCon.escape(room), function (err, result, fields){});
    delete rooms[room];
    console.log("Stanza "+room+" cancellata");
}

function disconnectRoom(room)
{
    let obj =
    {
        req: "disconnect"
    };
    json = JSON.stringify(obj);
    for(let i=0;i<rooms[room].length;i++)
    {
        let client = rooms[room][i];
        client.room = null;
        client.sendUTF(json);
        client.close();
        console.log("Chiuso");
    }
}

//Invia un disegno a tutti gli utenti in una stanza oltre al sender
function broadcastDrawing(room, username, sender, mode, x, y, lastX, lastY, color, width    )
{
    let obj =
    {
        from: username,
        req: mode,
        curX: x,
        curY: y,
        lastX: lastX,
        lastY: lastY,
        color: color,
        width: width
    };
    json = JSON.stringify(obj);
    for(let i=0;i<rooms[room].length;i++)
    {
        let client = rooms[room][i];
        if(client != sender)
        {
            console.log("Broadcast del disegno di "+username+" in stanza "+room);
            rooms[room][i].sendUTF(json);
        }
    }
}

//Invia una lista degli utenti connessi ad una stanza a tutti gli utenti connessi
function broadcastUsers(room)
{
    users = [];
    for(let i=0;i<rooms[room].length;i++)
    {
        let client = rooms[room][i];
        users.push(client.username);
        console.log(client.username);
    }
    console.log(users);
    let obj =
    {
        req: "updateUsers",
        users: users
    };
    json = JSON.stringify(obj);
    for(let i=0;i<rooms[room].length;i++)
    {
        let client = rooms[room][i];

        client.sendUTF(json);
        console.log("Broadcast della lista utenti della stanza "+room+" completato: ");
        console.log(users);
    }

}

function broadcastMessage(room, message)
{
    let obj =
    {
        req: "message",
        message: message
    };
    json = JSON.stringify(obj);
    for(let i=0;i<rooms[room].length;i++)
    {
        let client = rooms[room][i];
        client.sendUTF(json);
        console.log(client.username+"-/-"+json)
        console.log("Broadcast di una messaggio della stanza "+room+" completato:");
    }
}

//Controlla se un array è vuoto
function isEmpty(obj) {
    for(let key in obj) {
      if(obj.hasOwnProperty(key))
          return false;
    }
    return true;
}

//Invia al database la partecipazione di un utente a una stanza
function addPartToDatabase(user, room)
{
    //Converte il formato data da js a sql
    let date = new Date().toISOString().slice(0, 19).replace('T', ' ');
    myCon.query("INSERT INTO partecipations VALUES ('"+user+"','"+room+"','"+date+"')", function (err, result, fields)
    {
    });
    console.log("Inviata partecipazione di "+user+" alla stanza "+room+" in data "+date);
}







