<!doctype="html">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="style.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<body>

<?php
    $pdo = null;
    $dbHost = '127.0.0.1';
    $dbName = 'Baint';
    $dbUser = 'root';
    $dbPass = '';
    $dbCharset = 'utf8mb4';
    $dbOptions = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];
    $dsn = "mysql:host=$dbHost;dbname=$dbName;charset=$dbCharset";
    try
    {
        $pdo = new PDO($dsn, $dbUser, $dbPass, $dbOptions);
    }
    catch (\PDOException $e)
    {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
    session_start();
    //Controlla se è impostato un messaggio di sistema e stampa un allerta
    if(isset($_GET["message"]))
    {
        if($_GET["message"]=="logout")
        {
            unset($_SESSION["authToken"]);
            unset($_SESSION["username"]);
            $auth = false;
            echo "<script>
                    Swal.fire(
                      'Success',
                      'Logout eseguito con successo',
                      'success'
                    )
                  </script>";
        }
        if($_GET["message"]=="login")
        {
            echo "<script>
                    Swal.fire(
                      'Success',
                      'Login eseguito con successo',
                      'success'
                    )
                  </script>";
        }
        if($_GET["message"]=="loginFailed")
        {
            echo "<script>
                    Swal.fire(
                      'Errore',
                      'Login fallito',
                      'error'
                    )
                  </script>";
        }
        if($_GET["message"]=="roomDeleted")
        {
            echo "<script>
                    Swal.fire(
                      'Success',
                      'Stanza rimossa',
                      'success'
                    )
                  </script>";
        }
        if($_GET["message"]=="roomDeletedByOwner")
        {
            echo "<script>
                    Swal.fire(
                      'Kicked',
                      'Stanza rimossa dal creatore',
                      'error'
                    )
                  </script>";
        }

    }
    if(!isset($_SESSION["authToken"]))
    {
        $auth=false;
    }
    else
    {
        $auth=true;
    }
?>


<p hidden id="authToken" value="<?php if(isset($_SESSION["authToken"])){echo $_SESSION['authToken'];} ?>"></p>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="start.php"><img src="logo.png" class="icon" width="30" height="30"></img> Baint</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="/progetto_bello/info.php">Info</a>
        </li>
        <?php //Dropdown menu con lista delle ultime 10 stanze visitate
            if($auth)
            {
                echo '<li class="nav-item dropdown">
                         <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             Stanze recenti
                         </a>
                         <div class="dropdown-menu" aria-labelledby="navbarDropdown">';

                $stmt = $pdo->prepare("SELECT DISTINCT room FROM partecipations WHERE user= (SELECT username FROM users WHERE authToken=:authToken) ORDER BY timestamp LIMIT 10");
                $stmt->bindParam("authToken",$_SESSION["authToken"]);
                $stmt->execute();
                while($row = $stmt->fetch())
                {
                    $room = $row["room"];
                    echo '<a class="dropdown-item" href="paint.php?roomId='.$room.'">'.$room.'</a>';
                }
                echo '</div></li>';
            }
        ?>
    </ul>
    <ul class="navbar-nav navbar-right"
        <li class="nav-item">
            <form class="form-inline my-2 my-lg-0" method="GET">
                <input class="form-control mr-sm-0" name="search" type="search" placeholder="Cerca" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Cerca</button>
            </form>
        </li>
        <?php //Stampa i campi login e sign up se non loggato oppure i campi inserisci stanza e logout se loggato
            if(!$auth)
            {
                echo
                '<li class="nav-item"> <a href="register.php" class="btn btn-info my-2 my-sm-0 buttonspaced" role="button">Sign Up</a> </li>
                <div class="nav-item btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle buttonspaced pull-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</button>
                    <div class="dropdown-menu dropdown-login dropdown-menu-right">
                        <form class="px-2 py-0" method="POST" action="login.php">
                            <div class="form-group">
                                <label for="formUser">Username</label>
                                <input type="username" class="form-control" id="dropdownFormUsername" placeholder="Username" name="username">
                            </div>
                            <div class="form-group">
                                <label for="formPass">Password</label>
                                <input type="password" class="form-control" id="dropdownFormPassword" placeholder="Password" name="password">
                            </div>
                            <button type="submit" class="btn btn-primary">Sign in</button>
                        </form>
                    </div>
                </div>';
            }
            else
            {
                echo
                '<div class="nav-item btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle buttonspaced pull-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-plus">
                    </i> Nuova stanza</button>
                    <div class="dropdown-menu dropdown-login dropdown-menu-right">
                        <form class="px-2 py-0" method="POST" action="addRoom.php">
                            <div class="form-group">
                                <label for="formName">Nome</label>
                                <input type="name" class="form-control" id="dropdownFormName" placeholder="Nome" name="name">
                            </div>
                            <button type="submit" class="btn btn-primary">Crea stanza</button>
                        </form>
                    </div>
                </div>';
                echo
                '<li class="nav-item"> <form class="my-2 my-sm-0" method="GET"> <button type="submit" name="message" value="logout" class="btn btn-danger buttonspaced">Sign Out</button></form> </li>';
            }
        ?>
    </ul>
</nav>

<div class="container">
    <h1 class="my-4 text-center text-lg-left">Galleria delle stanze</h1>
    <div class="row text-center text-lg-left">
        <?php //Lista con tutte le stanze e le loro immagini, filtrate in base al campo di ricerca
            if(isset($_GET["search"]))
            {
                $argument = "%".$_GET["search"]."%";
                $stmt = $pdo->prepare("SELECT name, thumb FROM rooms WHERE name LIKE :search");
                $stmt->bindParam("search",$argument);
            }
            else
            {
                $stmt = $pdo->prepare("SELECT name, thumb FROM rooms");
            }
            $stmt->execute();
            $rooms = array();
            $thumbs = array();
            while($row = $stmt->fetch())
            {
                array_push($rooms, $row["name"]);
                array_push($thumbs, $row["thumb"]);
            }
            $isFirst = true;
            for($i = 0; $i<count($rooms); $i++)
            {
                if($isFirst)
                {
                    $index = mt_rand(0, count($rooms)-1);
                    $favroom = $rooms[$index];
                    echo
                    "<div class='col-lg-3 col-md-4 col-xs-6 text-center text-bold'>
                        <span class='text-center h6'> Random </span>
                        <a href='/progetto_bello/paint.php?roomId=$favroom' class='d-block mb-4 h-100'>
                            <i class='fas fa-random fa-10x' style='color: gray';></i>
                        </a>
                    </div>";
                }
                $isFirst = false;
                $name = $rooms[$i];
                $thumb = $thumbs[$i];
                echo
                "<div class='col-lg-3 col-md-4 col-xs-6 text-center text-bold'>
                    <span class='text-center h6'> $name </span>
                    <a href='/progetto_bello/paint.php?roomId=$name' class='d-block mb-4 h-100 rounded'>
                        <img class='img-fluid img-thumbnail' src='$thumb' width='400' height='300'>
                    </a>
                </div>";
            }

        ?>
    </div>
</div>
<!-- /.container -->
<!-- Footer -->
<footer class="footer py-3 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Baint 2017</p>
    </div>
    <!-- /.container -->
</footer>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
