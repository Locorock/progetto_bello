# Baint  <img src="logo.png" width="40">

## 1. Introduzione
Questa applicazione permette a diversi utenti di entrare in una stanza e disegnare contemporaneamente sullo stesso foglio. Questa piattaforma di disegno condiviso è pensata per essere utilizzata principalmente per motivi di svago date le limitazioni del disegno su canvas all'interno di un browser
All'interno dell'applicazione è possibile entrare in una stanza di disegno già creata oppure crearne una propria. Tutto questo è gestito attraverso l'homepage, che permette la visione del contenuto delle stanze già create sul sito. Le stanze mostrate sull'homepage possono essere filtrate tramite la funzione di ricerca in base al loro nome.    

<img src="docs/homepage.png" width="1366">

All'interno di una stanza è possibile disegnare con spessore (attraverso uno slide) e colore (con un color picker), pulire la canvas, scaricare il disegno o controllare gli utenti online. Inoltre se si è il proprietario della stanza è possibile cancellarla. Tutte queste azioni possono essere eseguite soltanto se si è registrati e loggati al sito.

<img src="docs/drawpage.png" width="1366">

## 2. Scopo dell'applicazione web

#### 1. Obiettivo

L'obiettivo di questa applicazione era quello di fornire un piattaforma di disegno condiviso semplice e con bassa latenza 

#### 2. Requisiti di partenza

L'applicazione per essere funzionale aveva bisogno di un sistema di stanze, un interfaccia di disegno rudimentale per i client e un server per condividere le azioni degli utenti in una stanza 

#### 3. Use case

Un utente può entrare nel portale, registrarsi e/o loggarsi su un proprio account e decidere se creare un propria stanza oppure entrare su una stanza già creata. Una volta in una stanza l'utente potrebbe invitare altri utenti a collaborare per esempio ad una sessione di brainstorming oppure continuare un disegno che altre persone hanno iniziato

#### 4. Funzionalità previste

Nell'applicazione completa erano previsti un sistema di autenticazione, un interfaccia per la creazione/selezione delle stanze e una canvas di disegno con diversi strumenti  

## 3. Progetto dell'applicazione web

#### 1. Definizione dei componenti

L'applicazione è composta da 5 componenti principali:

- Un interfaccia utente principale per l'autenticazione e la gestione delle stanze in HTML 5 e Bootstrap (frontend)
- Un interfaccia di disegno condiviso in JS
- Un server Apache con PHP per gestire le interazioni fra database e interfaccia utente  
- Un server database MySQL per gestire l'autenticazione e i vari dati necessari per il funzionamento dell'applicazione
- Un server di disegno in Node.js per gestire la condivisione dei disegni nelle stanze

#### 2. Interazione tra i componenti

Il frontend HTML interagisce con il server Apache PHP per gestire le richieste al database senza creare problematiche di sicurezza. L'interfaccia di disegno interagisce invece soltanto con il server di disegno, sia per richieste/messaggi riguardo alla modifica della canvas che per richieste al database. Il server database è necessario per regolare l'autenticazione, registrare e fornire dati su stanze, utenti e partecipazione di questi ultimi alle stanze. 

Il frontend e PHP comunicano tramite HTTP con richieste GET e POST, mentre client e server di disegno utilizzano un WebSocket e comunicano tramite messaggi in JSON. PHP utilizza PDO per le query al database, mentre Node.js utilizza la libreria MySQL per eseguire simili funzioni. 

## 4. Implementazione dell'applicazione web

#### 1. Tecnologie utilizzate

1. **HTML 5** // Utilizzato per la canvas esclusiva ad esso
2. **PHP** // Utilizzato per la gestione delle pagine server-side
3. **JavaScript** // Utilizzato per la gestione del disegno su canvas
4. **Node.js** // Utilizzato per la creazione del server di disegno
5. **JSON** // Utilizzato da server e client di disegno per la comunicazione di informazioni
6. **Websockets** // Utilizzato per la connessione fra server e client di disegno

#### 2. Librerie esterne

1. Bootstrap 4.2.1 sotto licenza [MIT License](https://github.com/twbs/bootstrap/blob/master/LICENSE) // Utilizzato per la creazione dell'interfaccia utente
2. FontAwesome 5.5.0 sotto licenza [CC BY 4.0 License](https://creativecommons.org/licenses/by/4.0/) // Utilizzato per le icone di Bootstrap
3. SweetAlert 2  8.11.6 sotto licenza sconosciuta // Utilizzato per le allerte sul sito
4. JSColor 2.0.5 sotto licenza [GNU GPL license v3](http://www.gnu.org/licenses/gpl-3.0.txt) // Utilizzato per il color picker
5. WebSocket per Node.js sotto licenza Apache License, Version 2.0 // Utilizzato per l'implementazione di WebSocket server-side
6. Canvas per Node.js sotto licenza [MIT License](https://github.com/Automattic/node-canvas/blob/HEAD/src/bmp/LICENSE.md) // Utilizzato per la creazione di thumbnail delle stanze di disegno server-side

#### 3. Descrizione codice

Il codice è suddiviso in diversi file:

- start.php

  L'homepage del sito, questa pagina invia una richiesta al database cercando tutte le stanze disponibili in base al valore inserito nel form di ricerca, nel caso non sia specificato nulla richiede semplicemente tutte le stanze. Dopo aver eseguito la query imposta la pagina per mostrare i nomi e le immagini delle stanze trovate e fornisce un link per accedere ad ognuna

- paint.php

  La pagina principale per il disegno, questa pagina richiama lo script canvas.js e fornisce i pulsanti e la canvas che permettono all'utente di interagire con la stanza

- register.php

  La pagina che permette la registrazione di un utente, richiede email, username e una password ripetuta, ne controlla poi la validità e nel caso in cui tutti i campi inseriti siano validi invia una query al database che effettua la registrazione

- info.php

  Una pagina molto semplice che fornisce informazioni sul sito

- addRoom.php

  Uno script in PHP che riceve in POST una richiesta di aggiunta di una stanza, controlla la sessione utente e l'esistenza della stanza e invia al database una query che effettua l'aggiunta

- deleteRoom.php

  Uno script in PHP che riceve in POST una richiesta di rimozione di una stanza, controlla la sessione utente, l'esistenza della stanza, che l'utente sia il creatore della stanza e invia al database una query che ne effettua la cancellazione

- login.php

  Uno script in PHP che riceve in POST una richiesta di login, controlla username e password con una query al database e se corretti imposta la sessione con username e token

- canvas.js

  Lo script che gestisce il disegno e le interazioni con i pulsanti in paint.php, la funzione runWebSocket è la funzione principale che gestisce la connessione WebSocket con il server di disegno. Dopo aver stabilito una connessione client e server si scambiano messaggi JSON in formato: 

  ```json
  {req: richiesta, dato1: dato, ... datoX: dato}
  ```

  Dove richiesta consiste nel tipo di messaggio spedito e nei dati vengono inseriti i vari parametri della richiesta 

- server.js

  Il server di disegno in Node.js che dopo essere stato avviato aspetta per richieste di connessione WebSocket, dopo aver stabilito una connessione comunica con il client nello stesso modo in cui il client comunica con il server. Se riceve un messaggio di disegno oltre a spedire lo stesso messaggio in broadcast a tutti gli altri utenti in una stanza lo disegna in una canvas interna. Queste canvas interne vengono spedite al database ogni 30 secondi e vengono utilizzate come backup e come thumbnail per le stanze mostrate in paint.php. 

- style.css

  Un piccolo foglio di stile che modifica alcuni aspetti del layout del sito

###### Messaggi in JSON

| Sender | Request      | Parametri                                         | Descrizione                                                  |
| ------ | ------------ | ------------------------------------------------- | ------------------------------------------------------------ |
| Client | connect      | authToken, room                                   | Messaggio per stabilire le variabili di connessione con il server |
| Client | drawPath     | authToken, curX, curY, lastX, lastY, color, width | Messaggio che codifica un singolo tratto di disegno          |
| Server | path         | from, curX, curY, lastX, lastY, color, width      | Messaggio che codifica un singolo tratto di disegno da spedire in broadcast |
| Server | updateUsers  | users                                             | Messaggio che server per aggiornare la lista utenti del client |
| Server | disconnect   |                                                   | Messaggio per forzare la disconnessione di un client         |
| Server | updateCanvas | base64                                            | Messaggio per aggiornare la canvas di un client              |
| Server | message      | message                                           | Messaggio per notificare l'entrata o l'uscita di un utente dalla stanza |

#### 4. Crono-programma

| Attività          | Ore previste | Ore attuali |
| :---------------- | ------------ | ----------- |
| HTML+Bootstrap    | 15           | 25          |
| Javascript Canvas | 6            | 10          |
| Node.js Server    | 20           | 15          |
| PHP               | 15           | 8           |
| Database MySQL    | 5            | 2           |
| Totale            | 61           | 60          |

## 5. Conclusioni e sviluppi futuri

L'applicazione, per quanto funzionale, ha molti aspetti in cui potrebbe essere espansa. A partire dal disegno stesso che potrebbe giovare dall'aggiunta di strumenti oltre al pennello semplice, come il testo e figure geometriche. Il sistema a stanze è funzionale ma potrebbe essere utile espandere le possibilità dell'utente per quanto riguarda la creazione. Le stanze potrebbe essere private e solo su invito, oppure riservare l'accesso ad una lista amici. Sarebbe inoltre possibile creare un api per l'integrazione di una stanza di disegno in un widget da inserire in altre pagine. 
